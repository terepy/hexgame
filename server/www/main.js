import { resizeCanvas, UTF8ToString, setupWebGL } from './utils.js';

const attributes = [
    { name: 'pos', size: 4 },
    { name: 'fg', size: 4 },
    { name: 'bg', size: 4 },
    { name: 'uv', size: 2 },
    { name: 'center', size: 2 },
    { name: 'shape', size: 1 },
    { name: 'radius', size: 1 },
    { name: 'outline', size: 1 }
];
const vertexSize = attributes.reduce((sum, attr) => sum + attr.size, 0) * 4;
const ws = new WebSocket('ws://192.168.1.103:8091');

let instance = null;
let reloadWasm = false;
let loggedIn = false;
let aspectRatioLocation, memory, gl, program, texture;

ws.addEventListener('message', function (event) {
    event.data.arrayBuffer().then(buffer => {
        const message = new Uint8Array(buffer);
        const messagePtr = instance.exports.malloc(message.length);
        new Uint8Array(memory.buffer, messagePtr, message.length).set(message);
        instance.exports.handle_message(messagePtr, message.length);
        instance.exports.free(messagePtr, message.length);
    });
});

async function init_wasm() {
    const response = await fetchWasm();

    const importObject = {
        env: {
            sendBytes: (ptr, len) => {
                const bytes = new Uint8Array(memory.buffer, ptr, len);
                ws.send(bytes);
            },
            drawFrame: (ptr, len) => {
                if (!gl) { return; }
                const vertices = new Float32Array(memory.buffer, ptr, len * vertexSize / 4);
                gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
                gl.viewport(0, 0, canvas.width, canvas.height);
                gl.clearColor(0.2, 0.4, 0.8, 1.0);
                gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
                gl.drawArrays(gl.TRIANGLES, 0, vertices.length * 4 / vertexSize);
            },
            getUsername: (ptr, len) => {
                const storedUsername = localStorage.getItem('username') || [];
                const usernameArray = JSON.parse(storedUsername);
                const u8Array = new Uint8Array(memory.buffer, ptr, usernameArray.length);
                u8Array.set(usernameArray);
                return usernameArray.length;
            },
            setUsername: (ptr, len) => {
                loggedIn = true;
                const u8Array = new Uint8Array(memory.buffer, ptr, len);
                const usernameArray = Array.from(u8Array);
                localStorage.setItem('username', JSON.stringify(usernameArray));
            },
            reloadWasm: () => reloadWasm = true,
            consoleLog: (ptr, len) => {
                const bytes = new Uint8Array(memory.buffer, ptr, len);
                const message = UTF8ToString(ptr, len, memory);
                console.log(message);
            }
        }
    };

    const { instance: newInstance } = await WebAssembly.instantiateStreaming(response, importObject);
    instance = newInstance;
    memory = instance.exports.memory;
    instance.exports.init(loggedIn);
    instance.exports.screen_size(canvas.width, canvas.height);
}

async function fetchWasm() {
    const timestamp = new Date().getTime();
    return await fetch(`hexgame.wasm?t=${timestamp}`);
}

async function init() {
    await init_wasm();

    const canvas = document.getElementById('canvas');
    ({ gl, program, texture } = await setupWebGL(canvas));

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    const fontAtlasLocation = gl.getUniformLocation(program, 'uFontAtlas');
    gl.uniform1i(fontAtlasLocation, 0);

    const buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);

    let offset = 0;
    attributes.forEach(attr => {
        const location = gl.getAttribLocation(program, attr.name);
        gl.vertexAttribPointer(location, attr.size, gl.FLOAT, false, vertexSize, offset);
        gl.enableVertexAttribArray(location);
        offset += attr.size * 4;
    });

    resizeCanvas(instance, canvas, gl);
    window.addEventListener('resize', () => resizeCanvas(instance, canvas, gl));
    
    canvas.addEventListener('mousemove', (event) => {
        const rect = canvas.getBoundingClientRect();
        const x = (event.clientX - rect.left) / rect.width * canvas.width;
        const y = (event.clientY - rect.top) / rect.height * canvas.height;
        instance.exports.mouse_pos(x, y);
    });

    canvas.addEventListener('mousedown', (event) => instance.exports.button(event.button, true));
    canvas.addEventListener('mouseup', (event) => instance.exports.button(event.button, false));

    window.addEventListener('keydown', (event) => instance.exports.button(event.key.charCodeAt(0), true));
    window.addEventListener('keyup', (event) => instance.exports.button(event.key.charCodeAt(0), false));

    canvas.addEventListener('wheel', (event) => {
        const button = event.deltaY < 0 ? 3 : 4; // 3 for ScrollUp, 4 for ScrollDown
        instance.exports.button(button, true);
        instance.exports.button(button, false);
    });

    requestAnimationFrame(render);
}

async function render() {
    try {
        instance.exports.run(BigInt(Date.now()));
    } catch (e) {
        reloadWasm = true;
        await new Promise(resolve => setTimeout(resolve, 10000));
    }

    if (reloadWasm) {
        reloadWasm = false;
        await init_wasm();
    }
    
    aspectRatioLocation = gl.getUniformLocation(program, 'uAspectRatio');
    const aspectRatio = canvas.width / canvas.height;
    gl.uniform1f(aspectRatioLocation, aspectRatio);

    requestAnimationFrame(render);
}

init();
