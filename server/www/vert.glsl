uniform float uAspectRatio;
attribute vec4 pos;
attribute vec4 fg;
attribute vec4 bg;
attribute vec2 uv;
attribute vec2 center;
attribute float shape;
attribute float radius;
attribute float outline;
uniform sampler2D uFontAtlas;
varying float vOutline;
varying vec2 vUv;
varying vec2 vPos;
varying vec4 vFg;
varying vec2 vCenter;
varying float vShape;
varying float vRadius;
varying vec4 vBg;

void main() {
    gl_Position = pos;
    vUv = uv;
    vPos = pos.xy * vec2(uAspectRatio, 1.0);
    vCenter = center * vec2(uAspectRatio, 1.0);
    vShape = shape;
    vRadius = radius;
    vFg = fg;
    vBg = bg;
    vOutline = outline;
}
