precision mediump float;
varying vec2 vPos;
varying vec2 vUv;
varying vec4 vFg;
varying vec4 vBg;
varying vec2 vCenter;
varying float vShape;
varying float vRadius;
uniform sampler2D uFontAtlas;
varying float vOutline;
void main() {
    float alpha = texture2D(uFontAtlas, vUv).r;
    if (vShape == 1.0) {
        float dist = distance(vPos, vCenter);
        float circleFactor = smoothstep(vRadius - 0.002, vRadius, dist);
        alpha = 1.0 - circleFactor;
    }
    float outlineFactor = smoothstep(0.98, 1.0, vOutline);
    gl_FragColor = mix(vBg, vFg, max(alpha, outlineFactor));
}
