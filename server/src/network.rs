use lib::*;
use std::sync::mpsc::Sender;
use std::thread;
use std::process::Command;

use crate::command::ServerCommand;

fn start_nginx() -> std::io::Result<()> {
    Command::new("nginx")
        .arg("-p").arg(".")
        .arg("-c").arg("nginx/nginx.conf")
        .arg("-e").arg("nginx/error.log")
        .status()?;
    Ok(())
}

fn stop_nginx() -> std::io::Result<()> {
    Command::new("nginx")
        .arg("-p").arg(".")
        .arg("-c").arg("nginx/nginx.conf")
        .arg("-e").arg("nginx/error.log")
        .arg("-s").arg("quit")
        .status()?;
    Ok(())
}

pub fn start(commands: Sender<ServerCommand>) -> Result<(), Box<dyn std::error::Error>> {
    let commands2 = commands.clone();
    ctrlc::set_handler(move|| {
        if let Err(e) = stop_nginx() {
            println!("failed to stop nginx: {}",e);
        }
        commands.send(ServerCommand::Exit).unwrap();
    })?;
    start_nginx()?;

    thread::spawn(move||
        ws::Builder::new().with_settings(ws::Settings {
            //encrypt_server: true,
            tcp_nodelay: true,
            .. ws::Settings::default()
        }).build(move|sender| {
            println!("new connection");
            Connection {
                name: Name::default(),
                sender,
                commands: commands2.clone(),
            }
        }).expect("failed to build websocket")
            .listen("192.168.1.103:8091").expect("failed to listen on ip")
    );

    Ok(())
}

struct Connection {
    name: Name,
    sender: ws::Sender,
    commands: Sender<ServerCommand>,
    //action: Arc<Mutex<ActionPlan>>,
    //auth: Arc<RwLock<HashMap<Name, [u64; 4]>>>,
    //client_output: Sender<(Client, Name)>,
}

impl ws::Handler for Connection {
    fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
        let msg = if let ws::Message::Binary(x) = msg { x } else { return Ok(()) };
        if self.name.is_some() {
            match deserialize(&msg) {
                Ok(msg) => self.commands.send(ServerCommand::ClientPacket(self.name, msg)).unwrap(),
                Err(e) => println!("deserialize error: {}", e),
            }
        } else { //not logged in yet
            match deserialize::<Name>(&msg) {
                Ok(name) if name.is_some() => {
                    println!("logging in as {name}");
                    //TODO: authentication and validation
                    //let _ = self.client.close_with_reason(ws::CloseCode::Normal, "invalid name of password");
                    self.name = name;
                    self.sender.send(serialize(&LoginSuccess(self.name))).unwrap();
                    self.commands.send(ServerCommand::Login { name: self.name, sender: self.sender.clone() }).unwrap();
                }
                Err(_) | Ok(_) => {
                    println!("error logging in");
                    self.sender.send(serialize(&LoginFail("invalid auth encoding".to_string()))).unwrap();
                },
            }
        }
        Ok(())
    }
    
    fn on_error(&mut self, e: ws::Error) {
        println!("socket error: {:?}",e);
    }
    
    fn on_close(&mut self, _code: ws::CloseCode, _reason: &str) {
        println!("connection closed: {:?}", self.name);
        self.commands.send(ServerCommand::Disconnect(self.name)).unwrap();
    }
    
    fn on_timeout(&mut self, _event: ws::util::Token) -> ws::Result<()> {
        println!("connection timeout {}",self.name);
        //self.commands.send(ServerCommand::Disconnect(self.name)).unwrap();
        Ok(())
    }
}
