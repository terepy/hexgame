use lib::*;

#[derive(Debug,Clone)]
pub enum ServerCommand {
    Login{ name: Name, sender: ws::Sender, },
    Disconnect(Name),
    Exit,
    ClientPacket(Name, ClientMsg),
}
