use lib::*;
mod map_gen;
use map_gen::*;
mod command;
use command::ServerCommand;
mod network;

use std::time::{Instant, Duration, SystemTime};
use std::sync::mpsc::channel;
use std::thread;
use std::fs::File;
use std::collections::HashMap;

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (command_sender, commands) = channel();
    
    let map_radius = 6;
    let mut world = gen_map(map_radius);
    
    network::start(command_sender)?;
    let mut clients = HashMap::new();

    let mut last_client_update = SystemTime::now();

    let mut next_turn = Instant::now();
    let turn_time = 5.0;
    'main: loop {
        while let Ok(command) = commands.try_recv() {
            match command {
                ServerCommand::Login{ name, sender } => {
                    let mut pos;
                    while {
                        pos = rand_pos(hex(0, 0), map_radius);
                        !world.tile(pos).vacant() || world.tile(pos).ty != Floor
                    } {}
                    world.entities.entry(name).or_insert_with(|| Entity {
                        pos,
                        id: name,
                        life: MAX_LIFE,
                        action_progress: rand::random(),
                        ..Entity::default()
                    });
                    sender.send(serialize(&WorldState(world.clone())))?;
                    clients.insert(name, sender);
                }
                ServerCommand::Disconnect(name) => { let _ = clients.remove(&name); },
                ServerCommand::Exit => break 'main,
                ServerCommand::ClientPacket(name, msg) => {
                    match msg {
                        RequestState => if let Some(sender) = clients.get(&name) {
                            sender.send(serialize(&WorldState(world.clone())))?;
                        }
                        DoAction(action) => {
                            world.entities.get_mut(&name).unwrap().action_queue = action;
                        },
                    }
                }
            }
        }

        let now = Instant::now();
        if next_turn < now {
            if let Some(_t) = world.step_forward(turn_time) {
                //TODO: track unused time and speed up
                next_turn += Duration::from_nanos((turn_time * 1e9) as u64);
                let data = serialize(&WorldState(world.clone()));
                for client in clients.values() {
                    client.send(data.clone())?;
                }
            } else {
                //TODO: delay next turn
            }
        }
        
        //check for new versions of the client
        let t = File::open("www/hexgame.wasm")?.metadata()?.modified()?;
        if t > last_client_update {
            last_client_update = t;
            for client in clients.values() {
                client.send(serialize(&RefreshNotif))?;
            }
        }

        thread::sleep(Duration::from_millis(10));
    }
    
    Ok(())
}
