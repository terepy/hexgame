use lib::*;
use rand::prelude::*;
//use rand::seq::SliceRandom;
use rand::distributions::WeightedIndex;

pub fn rand_pos(origin: Pos, radius: i32) -> Pos {
    origin.in_range(radius).choose(&mut thread_rng()).unwrap()
}

/*pub fn rand_name() -> Name {
    let mut r = name("$");
    let chars = b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    for _ in 1..NAME_LEN {
        r.push(*chars.choose(&mut thread_rng()).unwrap() as char);
    }
    r
}*/

pub fn gen_map(map_radius: i32) -> Map {
    let mut map = Map::default();
    let mut rng = thread_rng();

    let tile_chances = [1,8,1];
    let ty_gen = WeightedIndex::new(tile_chances).unwrap();

    for pos in hex(0, 0).in_range(map_radius) {
        let ty = TileType::from_num(ty_gen.sample(&mut rng));
        map.tiles.insert(pos, ty);
    }

    map
}

