use math_lib::{vec2::*, vec4::*};
use crate::Game;
use hex_pos::Pos;

pub struct UiScalar(pub f32);
pub struct WorldScalar(pub f32);
pub struct UiCoord(pub Vec2<f32>);
pub struct WorldCoord(pub Vec2<f32>);

pub trait GlScalar {
    fn to_scalar_gl(&self, game: &Game) -> f32;
}

impl GlScalar for UiScalar {
    fn to_scalar_gl(&self, game: &Game) -> f32 {
        self.0 / game.screen_size.x as f32 * 2.0
    }
}

impl GlScalar for WorldScalar {
    fn to_scalar_gl(&self, game: &Game) -> f32 {
        self.0 * game.zoom
    }
}

pub trait GlPos {
    fn to_gl(&self, game: &Game) -> Vec4<f32>;
}

impl GlPos for UiCoord {
    fn to_gl(&self, game: &Game) -> Vec4<f32> {
        let pos = self.0 / game.screen_size.f32() * 2.0 - 1.0;
        vec4(pos.x, pos.y, 1.0 - game.layer as f32 * 0.01, 1.0)
    }
}

impl GlPos for WorldCoord {
    fn to_gl(&self, game: &Game) -> Vec4<f32> {
        let x = (self.0.x * game.zoom) / game.aspect_ratio();
        let y = -self.0.y * game.zoom;
        vec4(x, y, 1.0 - game.layer as f32 * 0.01 - 0.5, 1.0)
    }
}
impl GlPos for Vec4<f32> {
    fn to_gl(&self, _game: &Game) -> Vec4<f32> {
        *self
    }
}

pub trait ToUiScalar {
    fn ui(self) -> UiScalar;
}

impl ToUiScalar for f32 {
    fn ui(self) -> UiScalar {
        UiScalar(self)
    }
}

impl ToUiScalar for i32 {
    fn ui(self) -> UiScalar {
        UiScalar(self as f32)
    }
}

pub trait ToWorldScalar {
    fn world(self) -> WorldScalar;
}

impl ToWorldScalar for f32 {
    fn world(self) -> WorldScalar {
        WorldScalar(self)
    }
}

impl ToWorldScalar for i32 {
    fn world(self) -> WorldScalar {
        WorldScalar(self as f32)
    }
}

pub trait ToUiPos {
    fn ui(self) -> UiCoord;
}

impl ToUiPos for (f32, f32) {
    fn ui(self) -> UiCoord {
        let (x, y) = self;
        UiCoord(vec2(x, y))
    }
}

impl ToUiPos for (i32, i32) {
    fn ui(self) -> UiCoord {
        let (x, y) = self;
        UiCoord(vec2(x, y).f32())
    }
}

impl ToUiPos for Vec2<f32> {
    fn ui(self) -> UiCoord {
        UiCoord(self)
    }
}

impl ToUiPos for Vec2<i32> {
    fn ui(self) -> UiCoord {
        UiCoord(self.f32())
    }
}

pub trait ToWorldPos {
    fn world(self) -> WorldCoord;
}

impl ToWorldPos for (f32, f32) {
    fn world(self) -> WorldCoord {
        let (x, y) = self;
        WorldCoord(vec2(x, y))
    }
}

impl ToWorldPos for (i32, i32) {
    fn world(self) -> WorldCoord {
        let (x, y) = self;
        WorldCoord(vec2(x, y).f32())
    }
}

impl ToWorldPos for Vec2<f32> {
    fn world(self) -> WorldCoord {
        WorldCoord(self)
    }
}

impl ToWorldPos for Vec2<i32> {
    fn world(self) -> WorldCoord {
        WorldCoord(self.f32())
    }
}

impl From<Pos> for WorldCoord {
    fn from(value: Pos) -> Self {
        value.to_world().world()
    }
}

impl GlPos for Pos {
    fn to_gl(&self, game: &Game) -> Vec4<f32> {
        self.to_world().world().to_gl(game)
    }
}
