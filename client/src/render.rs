use std::ops::{Deref, DerefMut};
use math_lib::{vec2::*, vec4::*};

use crate::Game;
use crate::gl_coord::*;
use crate::text::ATLAS_SIZE;

fn blend<T, const N: usize>(a: T, b: T, c: T) -> T
where
    T: Deref<Target = [f32; N]> + DerefMut + Default + Copy,
{
    let mut result = T::default();
    let r: &mut [f32; N] = result.deref_mut();
    let a: &[f32; N] = a.deref();
    let b: &[f32; N] = b.deref();
    let c: &[f32; N] = c.deref();
    for i in 0..N {
        r[i] = a[i] * c[i] + b[i] * (1.0 - c[i]);
    }
    result
}

#[repr(C)]
#[derive(Default,Copy,Clone)]
pub struct Vertex {
    pub pos: Vec4<f32>,
    pub fg: Vec4<f32>,
    pub bg: Vec4<f32>,
    pub uv: Vec2<f32>,
    pub center: Vec2<f32>,
    pub shape: f32,  // 0.0 for atlas, 1.0 for circle
    pub radius: f32,
    pub outline: f32,
}

impl Game {
    pub fn window(&mut self, pos: Vec2<i32>, size: Vec2<i32>, color: Vec4<f32>) -> Window {
        self.draw_rect_filled(pos.ui(), (pos + size).ui(), color);
        self.layer += 1;
        Window { game: self }
    }

    pub fn aspect_ratio(&self) -> f32 {
        self.screen_size.x as f32 / self.screen_size.y as f32
    }

    pub fn world_hex(&mut self, pos: impl Into<(f32, f32)>, scale: f32, color: Vec4<f32>, outline: f32) {
        let pos = pos.into();
        let bg = color; //color
        let fg = vec4(0.0, 0.0, 0.0, 1.0); //outline
        let corners = lib::hex_corners(pos, scale);
        for i in 0..6 {
            self.vertices.push(Vertex { pos: Vec2::from(pos).world().to_gl(self), fg, bg, ..Default::default() });
            self.vertices.push(Vertex { pos: corners[i].world().to_gl(self), fg, bg, outline, ..Default::default() });
            self.vertices.push(Vertex { pos: corners[(i + 1) % 6].world().to_gl(self), fg, bg, outline, ..Default::default() });
        }
    }

    pub fn draw_quad(&mut self, p1: impl GlPos, p2: impl GlPos, uv1: impl Into<(i32, i32)>, uv2: impl Into<(i32, i32)>, fg: Vec4<f32>, bg: Vec4<f32>) {
        let (p1, p2) = (p1.to_gl(self), p2.to_gl(self));
        let uv1 = Vec2::from(uv1.into()).f32() / ATLAS_SIZE.f32();
        let uv2 = Vec2::from(uv2.into()).f32() / ATLAS_SIZE.f32();
        for (x, y) in [(0i32, 0), (0, 1), (1, 0), (1, 0), (0, 1), (1, 1)] {
            let c = vec4(x, y, 1, 1).f32();
            let pos = blend(p1, p2, c);
            let uv = blend(uv1, uv2, vec2(x, y).f32());
            self.vertices.push(Vertex { pos, fg, bg, uv, ..Default::default() });
        }
    }

    pub fn draw_circle(&mut self, pos: impl GlPos, radius: impl GlScalar, color: Vec4<f32>) {
        let radius = radius.to_scalar_gl(self);
        let size = vec2(radius, radius);
        let fg = color;
        let bg = vec4(color.x, color.y, color.z, 0.0);
        let pos = pos.to_gl(self);
        let center = vec2(pos.x, pos.y);
        for p in [vec2(-1i32, -1), vec2(-1, 1), vec2(1, -1), vec2(1, -1), vec2(-1, 1), vec2(1, 1)] {
            let pos = pos + vec4(p.x as f32 * size.x, p.y as f32 * size.y, 0.0, 0.0);
            self.vertices.push(Vertex { pos, fg, bg, uv: vec2(0.0, 0.0), center, shape: 1.0, radius: radius, ..Default::default() });
        }
    }

    pub fn draw_rect_filled(&mut self, p1: impl GlPos, p2: impl GlPos, color: Vec4<f32>) {
        self.draw_quad(p1, p2, (0, 0), (0, 0), color, color);
    }
    
    pub fn draw_arrow(&mut self, start: impl Into<(f32, f32)>, end: impl Into<(f32, f32)>, color: Vec4<f32>) {
        let head_length = 0.2;
        let head_width = head_length * 0.5;
        let line_width = 0.025;
        let start = Vec2::from(start.into());
        let end = Vec2::from(end.into());
        let dir = (end - start).normalize();
        let perp = vec2(-dir.y, dir.x);

        let d = dir * head_length;
        let l = perp * line_width;
        let a = perp * head_width;

        let tail = [
            start + l,
            start - l,
            end + l - d,
            end - l - d,
        ].map(|p| p.world().to_gl(self));

        self.vertices.push(Vertex { pos: tail[0], fg: color, bg: color, ..Default::default() });
        self.vertices.push(Vertex { pos: tail[1], fg: color, bg: color, ..Default::default() });
        self.vertices.push(Vertex { pos: tail[2], fg: color, bg: color, ..Default::default() });

        self.vertices.push(Vertex { pos: tail[1], fg: color, bg: color, ..Default::default() });
        self.vertices.push(Vertex { pos: tail[2], fg: color, bg: color, ..Default::default() });
        self.vertices.push(Vertex { pos: tail[3], fg: color, bg: color, ..Default::default() });

        let head = [
            end,
            end + a - d,
            end - a - d,
        ].map(|p| Vertex { pos: p.world().to_gl(self), fg: color, bg: color, ..Default::default() });
        self.vertices.extend(head);
   }
}

pub struct Window<'a> {
    pub game: &'a mut Game,
}

impl<'a> Deref for Window<'a> {
    type Target = Game;

    fn deref(&self) -> &Self::Target {
        &self.game
    }
}

impl<'a> DerefMut for Window<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.game
    }
}

impl<'a> Drop for Window<'a> {
    fn drop(&mut self) {
        if self.layer > 0 {
            self.layer -= 1;
        }
    }
}
