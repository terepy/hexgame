#![allow(clippy::missing_safety_doc)]
#![feature(panic_payload_as_str,decl_macro)]

mod js_interface;
mod input;
mod text;

use input::{Button, UserInput};

pub use js_interface::*; //re-export for wasm
use math_lib::vec4::*;
use lib::*;

mod render;
mod gl_coord;
mod char_spec;
use render::Vertex;
use gl_coord::*;

#[derive(Default)]
pub struct Game {
    pub vertices: Vec<Vertex>,
    pub logged_in: bool,
    pub map: Map,
    pub zoom: f32,
    pub screen_size: Vec2<i32>,
    pub my_name: Name,
    pub input: UserInput,
    pub now: u64,
    pub turn_end: u64,
    pub layer: u32,
}


impl Game {
    pub fn init(&mut self) {
        self.zoom = 0.17;
        self.screen_size = vec2(1920, 1080);
        if let Some(name) = get_username() {
            self.my_name = name;
            if !self.logged_in {
                self.logged_in = true;
                login(name);
            }
        } else {
            log!("no saved username");
        }
    }

    pub fn run(&mut self, now: u64) {
        self.layer = 10;
        self.now = now;
        self.vertices.clear();
        if !self.logged_in {
            self.my_name = Name::from("a").unwrap();
            login(self.my_name);
            self.logged_in = true;
            return;
        }

        let mut player = self.map.entities.get(&self.my_name).cloned().unwrap_or_default();

        for (&pos, &tile) in self.map.tiles.clone().iter() {
            let col = match tile {
                TileType::Void => continue,
                TileType::Floor => vec4(0.2, 0.2, 0.2, 1.0),
                TileType::Wall => vec4(0.5, 0.5, 0.5, 1.0),
            };
            self.world_hex(pos.to_world(), 1.0, col, 1.02);
        }
        
        let movement_controls = [
            (Button::D, Right), (Button::E, UpRight), (Button::Q, UpLeft),
            (Button::A, Left), (Button::Z, DownLeft), (Button::C, DownRight),
        ];
        for (key, dir) in movement_controls {
            let p = player.pos + dir;
            if self.map.tile(p).vacant() {
                self.layer += 1;
                self.world_char(p.to_world(), 0.2, key.to_char().map(|c| c.to_ascii_uppercase()).unwrap_or('?'), Vec4::one(), text::Align::Center);
                self.layer -= 1;
            }
            if self.input.pressed.contains(&(key, true)) {
                player.action_queue.dir = dir;
                if player.action_queue.ty == Idle { player.action_queue.ty = Walk; }
            }
        }
        if self.input.pressed.contains(&(Button::ScrollUp, true)) {
            self.zoom *= 1.1;
        }
        if self.input.pressed.contains(&(Button::ScrollDown, true)) {
            self.zoom /= 1.1;
        }
        self.zoom = self.zoom.clamp(0.05, 2.0);

        for (name, e) in self.map.entities.clone().iter() {
            let entity_color = if *name == self.my_name {
                vec4(0.0, 0.9, 0.9, 1.0)
            } else {
                vec4(0.9, 0.3, 0.3, 1.0)
            };
            self.layer += 2;
            self.draw_circle(e.pos, 0.35.world(), entity_color);
            self.layer -= 1;
            //draw a yellow arrow for in-progress movement
            if let Some(d) = e.action.movement() {
                let from = e.pos.to_world();
                let to = (e.pos + d).to_world();
                self.draw_arrow(from, to, vec4(1.0, 1.0, 0.0, 1.0)); // yellow arrow
            }
            // draw a gray arrow for planned movement
            if let Some(d) = e.action_queue.movement() {
                let from = e.pos.to_world();
                let to = (e.pos + d).to_world();
                self.draw_arrow(from, to, vec4(1.5, 0.5, 0.5, 1.0)); // gray arrow
            }
            //shade attacked hexes in red based on action progress
            for attack in e.action.attacks(e.pos) {
                self.world_hex(attack.pos.to_world(), 1.0, vec4(0.5, 0.2, 0.2, 0.25), 0.0);
                //extra growing hex based on how soon the attack will be
                self.world_hex(attack.pos.to_world(), e.action_progress, vec4(1.0, 0.2, 0.2, 0.25), 0.0);
            }
            for attack in e.action_queue.attacks(e.pos) {
                self.world_hex(attack.pos.to_world(), 1.0, vec4(0.5, 0.2, 0.2, 0.25), 0.0);
            }
            self.layer -= 1;
        }

        let time_left = (self.turn_end - self.now) as f32 / 5000.0;
        self.draw_rect_filled(vec2(0.0, 0.0).ui(), (vec2(time_left, 0.01) * self.screen_size.f32()).ui(), vec4(0.0, 0.0, 1.0, 1.0));

        if self.map.entities.get(&self.my_name).is_some_and(|e| e.action_queue != player.action_queue) {
            send_msg(ClientMsg::DoAction(player.action_queue));
            self.map.entities.get_mut(&self.my_name).unwrap().action_queue = player.action_queue;
        }

        self.input.tick();
        
        // Sort vertices by z-coordinate (groups of 3 vertices per triangle)
        let chunk_size = 3;
        let mut chunks: Vec<[Vertex; 3]> = self.vertices.chunks_exact(chunk_size).map(|a| a.try_into().unwrap()).collect();
        chunks.sort_by(|a, b| {
            let a_z = a[0].pos.z;
            let b_z = b[0].pos.z;
            b_z.partial_cmp(&a_z).unwrap_or(std::cmp::Ordering::Equal)
        });
        
        let sorted_vertices: Vec<Vertex> = chunks.into_iter().flatten().collect();
        draw_frame(&sorted_vertices);
    }

    pub fn handle_message(&mut self, message: ServerMsg) {
        match message {
            ServerMsg::LoginSuccess(name) => {
                self.logged_in = true;
                self.my_name = name;
            }
            ServerMsg::WorldState(map) => {
                self.map = map;
                self.map.step_forward(10.0); // ensure the version we edit is up to date
                //TODO: replay
                self.turn_end = self.now + 5000;
            }
            ServerMsg::RefreshNotif => reload_wasm(),
            _ => {}
        }
    }
}
