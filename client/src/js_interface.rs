use std::{ffi::CString, mem::MaybeUninit, alloc::{alloc, dealloc, Layout}};

use lib::{deserialize, serialize, ClientMsg, Name};
use math_lib::vec2::*;

pub macro log($($arg:tt)*) {
    crate::console_log(&format!($($arg)*))
}

use crate::{input::Button, Game};
use crate::render::Vertex;

static mut GAME: MaybeUninit<Game> = MaybeUninit::uninit();

#[no_mangle]
pub unsafe extern "C" fn init(logged_in: bool) {
    std::panic::set_hook(Box::new(|info| {
        console_log(&format!("panic: {}",info.payload_as_str().unwrap_or_default()));
    }));
    GAME = MaybeUninit::new(Game::default());
    GAME.assume_init_mut().logged_in = logged_in;
    GAME.assume_init_mut().init();
}


#[no_mangle]
pub unsafe extern "C" fn run(now: u64) {
    GAME.assume_init_mut().run(now);
}

#[no_mangle]
pub unsafe extern "C" fn handle_message(message: *mut u8, message_len: usize) -> *const u8 {
    let m = std::slice::from_raw_parts(message, message_len);
    match deserialize(m) {
        Ok(m) => {
            GAME.assume_init_mut().handle_message(m);
            let result = CString::new(format!("{:?}", GAME.assume_init_ref().map.tiles.len())).unwrap();
            let r = result.as_ptr() as *const u8;
            std::mem::forget(result);
            r
        }
        Err(e) => {
            let result = CString::new(format!("Error deserializing message: {}, raw: {:?}", e, m)).unwrap();
            let r = result.as_ptr() as *const u8;
            std::mem::forget(result);
            r
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn malloc(size: usize) -> *mut u8 {
    let layout = Layout::array::<u8>(size).unwrap_or_else(|_| Layout::new::<u8>());
    alloc(layout)
}

#[no_mangle]
pub unsafe extern "C" fn free(ptr: *mut u8, size: usize) {
    if ptr.is_null() { return; }
    let layout = Layout::array::<u8>(size).unwrap_or_else(|_| Layout::new::<u8>());
    dealloc(ptr, layout);
}

#[no_mangle]
pub unsafe extern "C" fn screen_size(x: i32, y: i32) {
    GAME.assume_init_mut().screen_size = vec2(x, y);
}

#[no_mangle]
pub unsafe extern "C" fn mouse_pos(x: f32, y: f32) {
    GAME.assume_init_mut().input.mouse_pos = vec2(x, y);
}

#[no_mangle]
pub unsafe extern "C" fn button(button: u8, pressed: bool) {
    if let Ok(button) = Button::try_from(button) {
        GAME.assume_init_mut().input.pressed.push((button, pressed));
    }
}

#[no_mangle]
pub unsafe extern "C" fn is_logged_in() -> bool {
    GAME.assume_init_ref().logged_in
}

extern "C" {
    fn sendBytes(ptr: *const u8, len: usize);
    fn drawFrame(ptr: *const Vertex, len: usize);
    fn getUsername(ptr: *mut u8) -> usize;
    fn setUsername(ptr: *const u8, len: usize) -> usize;
    fn reloadWasm();
    fn consoleLog(ptr: *const i8, len: usize);
}

pub fn send_msg(msg: ClientMsg) {
    let bytes = serialize(&msg);
    unsafe { sendBytes(bytes.as_ptr(), bytes.len()); }
}

pub fn login(name: Name) {
    let bytes = serialize(&name);
    unsafe {
        sendBytes(bytes.as_ptr(), bytes.len());
        setUsername(bytes.as_ptr(), bytes.len());
    }
}

pub fn draw_frame(vertices: &[Vertex]) {
    unsafe { drawFrame(vertices.as_ptr(), vertices.len()); }
}

pub fn get_username() -> Option<Name> {
    let mut bytes = Box::new([0u8; 32]);
    let len = unsafe { getUsername(bytes.as_mut_slice().as_mut_ptr()) };
    deserialize(&bytes[..len]).ok().filter(|n: &Name| !n.is_empty())
}

pub fn console_log(message: &str) {
    let c_string = CString::new(message).unwrap();
    unsafe {
        consoleLog(c_string.as_ptr(), c_string.to_bytes().len());
    }
}


pub fn reload_wasm() {
    unsafe { reloadWasm(); }
}
