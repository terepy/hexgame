use math_lib::{vec2::*, vec4::*};
use crate::Game;
use crate::gl_coord::*;
use crate::char_spec::CHARS;

pub const ATLAS_SIZE: Vec2<i32> = vec2(256, 512);
const FONT_SCALE_MUL: f32 = 1.0 / 32.0;

#[derive(Debug, Clone, Copy)]
pub struct CharSpec {
    pub uv_start: Vec2<i32>,
    pub size: Vec2<i32>,
    pub offset: Vec2<f32>,
    pub advance: f32,
}

#[derive(Copy,Clone)]
pub enum Align {
    TopLeft,
    TopCenter,
    TopRight,
    CenterLeft,
    Center,
    CenterRight,
    BottomLeft,
    BottomCenter,
    BottomRight,
}

impl Game {
    pub fn world_char(&mut self, pos: impl Into<Vec2<f32>>, size: f32, mut c: char, col: Vec4<f32>, alignment: Align) -> f32 {
        let mut pos: Vec2<f32> = pos.into();
        if c as usize > CHARS.len() {
            c = '?';
        }
        let spec = CHARS[c as usize];
        let scale = FONT_SCALE_MUL * size;

        match alignment {
            Align::TopCenter | Align::Center | Align::BottomCenter => pos.x -= spec.size.f32().x * scale / 2.0,
            Align::TopRight | Align::CenterRight | Align::BottomRight => pos.x -= spec.size.f32().x * scale,
            _ => {}
        }

        match alignment {
            Align::CenterLeft | Align::Center | Align::CenterRight => pos.y -= spec.size.f32().y * scale / 2.0,
            Align::BottomLeft | Align::BottomCenter | Align::BottomRight => pos.y -= spec.size.f32().y * scale,
            _ => {}
        }

        let p1 = pos + spec.offset.f32() * scale;
        let p2 = p1 + spec.size.f32() * scale;
        self.draw_quad(
            p1.world().to_gl(self),
            p2.world().to_gl(self),
            spec.uv_start,
            spec.uv_start + spec.size,
            col,
            Vec4::zero(),
        );
        CHARS[c as usize].advance
    }

    pub fn world_text(&mut self, pos: impl Into<Vec2<f32>>, size: f32, text: &str, col: Vec4<f32>, alignment: Align) {
        let mut pos: Vec2<f32> = pos.into();

        let total_width: f32 = text.chars().map(|c| {
            let spec = CHARS[c as usize];
            spec.advance * FONT_SCALE_MUL * size
        }).sum();

        match alignment {
            Align::TopCenter | Align::Center | Align::BottomCenter => pos.x -= total_width / 2.0,
            Align::TopRight | Align::CenterRight | Align::BottomRight => pos.x -= total_width,
            _ => {}
        }

        let line_height = FONT_SCALE_MUL * size; //eventually need to change this for text wrapping
        match alignment {
            Align::CenterLeft | Align::Center | Align::CenterRight => pos.y -= line_height / 2.0,
            Align::BottomLeft | Align::BottomCenter | Align::BottomRight => pos.y -= line_height,
            _ => {}
        }

        for c in text.chars() {
            pos.x += self.world_char(pos, size, c, col, alignment);
        }
    }
}
