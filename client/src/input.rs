use std::collections::HashSet;
use math_lib::vec2::*;

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Button {
    LMouse = 0,
    MMouse = 1,
    RMouse = 2,
    ScrollUp = 3,
    ScrollDown = 4,
    Zero = 48,
    One = 49,
    Two = 50,
    Three = 51,
    Four = 52,
    Five = 53,
    Six = 54,
    Seven = 55,
    Eight = 56,
    Nine = 57,
    A = 97,
    B = 98,
    C = 99,
    D = 100,
    E = 101,
    F = 102,
    G = 103,
    H = 104,
    I = 105,
    J = 106,
    K = 107,
    L = 108,
    M = 109,
    N = 110,
    O = 111,
    P = 112,
    Q = 113,
    R = 114,
    S = 115,
    T = 116,
    U = 117,
    V = 118,
    W = 119,
    X = 120,
    Y = 121,
    Z = 122,
}

impl TryFrom<u8> for Button {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        use Button::*;
        if value < 5 {
            Ok([LMouse, MMouse, RMouse, ScrollUp, ScrollDown][value as usize])
        } else if value > 47 && value < 58 {
            Ok([Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine][value as usize - 48])
        } else if value > 96 && value < 123 {
            Ok([A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z][value as usize - 97])
        } else {
            Err(())
        }
    }
}

impl Button {
    pub fn to_char(self) -> Option<char> {
        if (self as u32) >= 32 {
            Some(char::try_from(self as u32).unwrap())
        } else {
            None
        }
    }
}

#[derive(Default)]
pub struct UserInput {
    pub old_mouse_pos: Vec2<f32>,
    pub mouse_pos: Vec2<f32>,
    pub held: HashSet<Button>,
    pub pressed: Vec<(Button, bool)>, //true = pressed, false = released
}

impl UserInput {
    pub fn tick(&mut self) {
        self.old_mouse_pos = self.mouse_pos;
        for (key, pressed) in self.pressed.drain(..) {
            if pressed {
                self.held.insert(key);
            } else {
                self.held.remove(&key);
            }
        }
    }

    pub fn clicked(&self) -> bool {
        self.pressed.contains(&(Button::LMouse, true))
    }
}
