use crate::*;

#[derive(Copy,Clone,Debug,Serialize,Deserialize,Eq,PartialEq)]
pub struct Action {
    pub dir: Dir,
    pub ty: ActionTy,
}

impl Action {
    pub fn movement(&self) -> Option<Dir> {
        Some(match self.ty {
            Idle | Cleave => None?,
            Walk | SwordDraw => self.dir,
            Skewer => -self.dir,
        })
    }

    pub fn attacks(&self, from: Pos) -> Vec<Attack> {
        let mut r: Vec<Attack> = match self.ty {
            Idle | Walk => vec![],
            Cleave => (0..6).map(Dir::from).filter(|&d| d != -self.dir).map(|d| (from + d, Some(d))).collect(),
            Skewer => vec![(from + self.dir, Some(-self.dir)), (from + self.dir + self.dir, Some(-self.dir))],
            SwordDraw => (3..6).map(|i| (from + Pos::from(self.dir) * i, Some(self.dir))).collect(),
        }.into_iter().map(|(pos, kb)| Attack { pos, kb, .. Attack::default() }).collect();
        for attack in &mut r {
            attack.damage = self.base_damage();
            attack.halt_on_hit = self.ty == SwordDraw;
        }
        r
    }

    pub fn base_damage(&self) -> f32 {
        match self.ty {
            Idle | Walk => 0.0,
            Cleave => 5.0,
            Skewer => 8.0,
            SwordDraw => 6.0,
        }
    }
}

impl Default for Action {
    fn default() -> Self {
        Self {
            dir: Right,
            ty: Idle,
        }
    }
}

#[derive(Copy,Clone,Debug,Serialize,Deserialize,Eq,PartialEq)]
pub enum ActionTy {
    Idle,
    Walk,
    Cleave,
    Skewer,
    SwordDraw,
}
pub use ActionTy::*;
