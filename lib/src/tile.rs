use crate::*;

#[derive(Copy,Clone,Debug,Serialize,Deserialize)]
pub struct Tile {
    pub ty: TileType,
    pub entity: Name,
}

impl Default for Tile {
    fn default() -> Self {
        Self::new()
    }
}

impl Tile {
    pub const fn new() -> Self {
        Self {
            ty: Void,
            entity: Name::new_const(),
        }
    }
    
    pub fn vacant(&self) -> bool {
        self.entity.is_none() && self.ty.walkable()
    }
}

pub const NUM_TILE_TYPES: usize = 3;
#[derive(Copy,Clone,Debug,Serialize,Deserialize,Eq,PartialEq,Default)]
pub enum TileType {
    #[default]
    Void = 0,
    Floor = 1,
    Wall = 2,
}
pub use TileType::*;

impl TileType {
    pub fn walkable(&self) -> bool {
        match self {
            Void => true, //you *can* walk onto and off of void, you just take damage and get teleported
            Floor => true,
            Wall => false,
        }
    }
    
    pub fn from_num(x: usize) -> Self {
        match x {
            0 => Void,
            1 => Floor,
            _ => Wall,
        }
    }
    
    pub fn name(&self) -> &'static str {
        match self {
            Void => "void",
            Floor => "floor",
            Wall => "wall",
        }
    }
}
