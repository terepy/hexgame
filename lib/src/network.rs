use crate::*;

#[derive(Debug,Clone,Serialize,Deserialize)]
pub enum ServerMsg {
    WorldState(Map),
    //WorldUpdate{ actor: Name, action: Action, timer: f64 },
    LoginSuccess(Name),
    LoginFail(String),
    RefreshNotif, //an update is available, client should refresh
}
pub use ServerMsg::*;

#[derive(Debug,Clone,Serialize,Deserialize)]
pub enum ClientMsg {
    RequestState,
    DoAction(Action),
}
pub use ClientMsg::*;
