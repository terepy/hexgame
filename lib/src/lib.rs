use std::error::Error;
use std::io::{Read,Write};

pub use serde::{Serialize,Deserialize};
pub use hex_pos::*;
pub use math_lib::vec2::*;

mod tile;
mod map;
mod entity;
mod network;
mod action;

pub use entity::*;
pub use tile::*;
pub use map::*;
pub use network::*;
pub use action::*;

pub const NAME_LEN: usize = 31;
pub type Name = arrayvec::ArrayString<NAME_LEN>;

pub trait IsNone {
    fn is_none(&self) -> bool;
    fn is_some(&self) -> bool { !self.is_none() }
}

impl IsNone for Name {
    fn is_none(&self) -> bool {
        self.is_empty()
    }
}

pub fn name<S: AsRef<str>>(s: S) -> Name {
    let s: &str = s.as_ref();
    Name::from(s).unwrap_or_else(|_| panic!("name too long: {},{}", s.len(), s))
}

fn ron_config() -> ron::ser::PrettyConfig {
    ron::ser::PrettyConfig::default()
        .indentor("\t".into())
        .depth_limit(4)
}

pub fn serialize<T: Serialize>(x: &T) -> Vec<u8> {
    ron::ser::to_string_pretty(x, ron_config()).unwrap().into_bytes()
}

pub fn deserialize<T: serde::de::DeserializeOwned>(x: &[u8]) -> Result<T, Box<dyn Error>> {
    Ok(ron::de::from_bytes(x)?)
}

pub fn ron_ser_into<T: Serialize, W: Write>(w: W, x: &T) -> Result<(), Box<dyn Error>> {
    Ok(ron::ser::to_writer_pretty(w, x, ron_config())?)
}

pub fn ron_de_from<T: serde::de::DeserializeOwned, R: Read>(r: R) -> Result<T, Box<dyn Error>> {
    Ok(ron::de::from_reader(r)?)
}
