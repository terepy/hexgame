use crate::*;

pub const COLLISION_DAMAGE: f32 = 2.0;
pub const MAX_LIFE: f32 = 60.0;

#[derive(Clone,Default,Debug,Serialize,Deserialize)]
pub struct Entity {
    pub id: Name,
    pub pos: Pos,
    pub life: f32,
    pub action_progress: f32,
    pub action: Action,
    pub action_queue: Action,
    pub ai: Option<EntityAi>,
}

impl Entity {
    pub fn time_to_act(&self) -> f32 {
        1.0 - self.action_progress
    }

    pub fn step_forward(&mut self, dt: f32) {
        self.action_progress += dt;
    }

    pub fn act(&mut self, map: &mut Map) {
        self.action_progress = 0.0;
        let attacks = self.action.attacks(self.pos);
        if let Some(d) = self.action.movement() {
            self.try_move(map, d);
        }
        for atk in attacks {
            if atk.act(map) && atk.halt_on_hit {
                break;
            }
        }
        self.action = self.action_queue;
        if let Some(ai) = self.ai.take() {
            let a = ai.choose_action(self, map);
            self.action_queue = a;
            self.ai = Some(ai);
        } else {
            self.action_queue.ty = Idle;
        }
    }

    //where they will be when their action finishes
    pub fn dest_pos(&self) -> Pos {
        self.action.movement().map(|d| self.pos + d).unwrap_or(self.pos)
    }

    pub fn take_damage(&mut self, amount: f32) {
        self.life -= amount;
    }

    pub fn try_move(&mut self, map: &mut Map, dir: Dir) -> bool {
        if map.tile(self.pos + dir).vacant() {
            self.pos += dir;
            true
        } else if let Some(e) = map.entity_at(self.pos + dir) {
            e.take_damage(COLLISION_DAMAGE);
            self.take_damage(COLLISION_DAMAGE * 2.0);
            false
        } else {
            self.take_damage(COLLISION_DAMAGE * 3.0);
            false
        }
    }
    
    pub fn visual_pos(&self) -> Vec2<f32> {
        let t = smoothstep(self.action_progress);
        Vec2::from(self.pos.to_world()) * (1.0 - t) + Vec2::from(self.dest_pos().to_world()) * t
    }
}

#[derive(Clone,Debug,Serialize,Deserialize)]
pub enum EntityAi {
    Golem,
}
use EntityAi::*;

impl EntityAi {
    fn choose_action(&self, e: &Entity, map: &Map) -> Action {
        match self {
            Golem => {
                let mut target = None;
                if let Some(e) = map.entities.values().next() {
                    target = Some(e);
                }
                if let Some(target) = target {
                    Action { dir: e.pos.dir_to(target.pos), ty: SwordDraw }
                } else {
                    Action { dir: e.action_queue.dir, ty: Idle }
                }
            },
        }
    }
}

fn smoothstep(x: f32) -> f32 {
    x * x * (3.0 - 2.0 * x)
}
