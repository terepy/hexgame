use std::collections::HashMap;
use crate::*;

#[derive(Default,Debug,Clone,Serialize,Deserialize)]
pub struct Map {
    pub tiles: HashMap<Pos, TileType>,
    pub entities: HashMap<Name, Entity>,
}

impl Map {
    pub fn tile(&self, pos: Pos) -> Tile {
        Tile {
            ty: self.tiles.get(&pos).cloned().unwrap_or_default(),
            entity: self.entities.values().find(|e| e.pos == pos).map(|e| e.id).unwrap_or_default(),
        }
    }

    //steps by `dt`, or until an action resolves, whichever is less, returns the amount not stepped if any
    pub fn step_forward(&mut self, dt: f32) -> Option<f32> {
        let mut step = dt;
        let mut actor = Name::default();
        if let Some(e) = self.entities.values_mut()
            .filter(|e| e.time_to_act() <= step)
            .min_by_key(|e| (e.time_to_act() * 1e10) as u64)
        {
            step = e.time_to_act();
            actor = e.id;
        }
        for entity in self.entities.values_mut() {
            if entity.id == actor { continue; }
            entity.step_forward(step);
        }
        if let Some(mut e) = self.entities.remove(&actor) {
            e.act(self);
            self.entities.insert(actor, e);
            //TODO: scan for deaths
            Some(dt - step)
        } else {
            None
        }
    }

    pub fn entity_at(&mut self, pos: Pos) -> Option<&mut Entity> {
        self.entities.values_mut().find(|e| e.pos == pos)
    }
    
    pub fn remove_entity_at(&mut self, pos: Pos) -> Option<Entity> {
        let id = self.entity_at(pos)?.id;
        self.entities.remove(&id)
    }
}

#[derive(Default,Copy,Clone,Debug,Serialize,Deserialize)]
pub struct Attack {
    pub pos: Pos,
    pub kb: Option<Dir>,
    pub damage: f32,
    pub halt_on_hit: bool,
}

impl Attack {
    pub fn act(self, map: &mut Map) -> bool {
        let mut e = if let Some(e) = map.remove_entity_at(self.pos) { e } else { return false };
        e.take_damage(self.damage);
        if let Some(d) = self.kb {
            e.try_move(map, d);
        }
        map.entities.insert(e.id, e);
        true
    }
}
